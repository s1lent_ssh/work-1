#include <memory>
#include <vector>
#include "print.cpp"

int main(int argc, const char* argv[]) {
    SnifferConfiguration config;
    config.set_immediate_mode(true);

    vector<string> params(argv + 1, argv + argc);

    if(params.size() == 2) 
        config.set_filter(params.at(1));

    unique_ptr<Sniffer> sniffer;

    try {
        sniffer = make_unique<Sniffer>(params.at(0), config);
    } catch (invalid_pcap_filter)  {
        cout << "Error: Input valid filter" << endl;
        return 1;
    } catch (pcap_error) {
        cout << "Error: No such interface" << endl;
        return 1;
    }

    sniffer->sniff_loop([](const Packet& packet) {
        const auto& pdu = packet.pdu();
        const auto& ip = pdu->rfind_pdu<IP>();
        print(ip);
        const auto& inner = ip.inner_pdu();
        switch(inner->pdu_type()) {
            case PDU::PDUType::TCP: 
                print(pdu->rfind_pdu<TCP>());
                break;
            case PDU::PDUType::UDP:
                print(pdu->rfind_pdu<UDP>());
                break;
            case PDU::PDUType::ICMP: 
                print(pdu->rfind_pdu<ICMP>());
                break;
            case PDU::PDUType::ARP:
                print(pdu->rfind_pdu<ARP>());
                break;
            default:
                printError("Not supported");
        }
        return true;
    });
}
