#include <iostream>
using namespace std;

namespace Color {

enum Code {
	RED = 31,
	GREEN = 32,
	YELLOW = 33,
	BLUE = 34,
	MAGENTA = 35,
	CYAN = 36,
	LIGHT_GRAY = 37,
	DEFAULT = 39
};

class Modifier {
	Code code;
public:
	Modifier(const Code& code) : code(code) {} 
	friend ostream& operator<<(ostream& os, const Modifier& m) {
		os << "\033[" << m.code << "m";
		return os;
	}
};

}
