#include <iostream>
#include <tins/tins.h>
#include "color.cpp"

using namespace Tins;
using namespace std;

Color::Modifier mod(Color::BLUE), def(Color::DEFAULT), mod2(Color::GREEN), mod3(Color::RED);

template<typename T>
void print(const T& data) { 
	cout << '\t' << mod << data.first << ":\t" << def << data.second << endl; 
}

template<typename T, typename... Args>
void print(const T& data, const Args&... args) {
	cout << mod << '\t' << data.first << ":\t" << def << data.second << endl;
	print(args...);
}

inline void printSubTitle(const string& value) { 
	cout << '\t' << mod2 << "["+ value + "]" << def << endl; 
}

inline void printTitle(const string& value) { 
	static int counter = 0;
	cout << def << counter++ << ". " << mod2 << value << def <<  endl; 
}

inline void printError(const string& error) { 
	cout << '\t' << mod3 << "[" << error << "]" << endl << def; 
}

void print(const IP& ip) {
	printTitle("IP");
	print(
		make_pair("Dest IP", ip.dst_addr()),
		make_pair("Source IP", ip.src_addr()),
		make_pair("Total length", ip.tot_len()),
		make_pair("TTL Value", to_string(ip.ttl()))
	);
}

void print(const TCP& tcp) {
	printSubTitle("TCP");
	print(
		make_pair("Dest port", tcp.dport()),
		make_pair("Source port", tcp.sport()),
		make_pair("Window size", tcp.window())
	);
}

void print(const UDP& udp) {
	printSubTitle("UDP");
	print(
		make_pair("Dest port", udp.dport()),
		make_pair("Source port", udp.sport()),
		make_pair("Total length", udp.length()),
		make_pair("Checksum", udp.checksum())
	);
}

void print(const ICMP& icmp) {
	printSubTitle("ICMP");
	switch(icmp.type()) {
		case ICMP::Flags::ECHO_REPLY:
			print(
				make_pair("Type", "\tEcho Reply")
			);
			break;
		case ICMP::Flags::ECHO_REQUEST:
			print(
				make_pair("Type", "\tEcho Request")
			);
			break;
	}
	print(
		make_pair("Checksum", icmp.checksum())
	);
}

void print(const ARP& arp) {
	printSubTitle("ARP");
	print(
		make_pair("Sender MAC", arp.sender_hw_addr()),
		make_pair("Target MAC", arp.target_hw_addr()),
		make_pair("Sender IP", arp.sender_ip_addr()),
		make_pair("Targer IP", arp.target_ip_addr())
	);
}


